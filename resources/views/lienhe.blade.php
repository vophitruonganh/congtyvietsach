@extends('layouts.index')

@section('content')
    <section id="features" class="features">
    <div class="slider_overlay">
        <div class="container">
            <div class="row">
                <div class="main_features_content_area  wow fadeIn" data-wow-duration="3s">
                    <div class="col-lg-12">
                        <div style="background: #000; opacity: 0.7; padding: 15px; border-top-left-radius: 15px; border-top-right-radius: 15px;">
                            <h3 class="text-center text-uppercase">Liên hệ với chúng tôi</h3>
                            <p class="text-bold text-center">{{ $diachi or 'Địa chỉ: K26, Ấp Mỹ Hoà 4, Xã Xuân Thới Đông, huyện Hóc Môn, thành phố Hồ Chí Mính' }}</p>
                            <p class="text-bold text-center">{{ $diachi or 'Giờ mở cửa: 8h - 18h (các ngày trong tuần)' }}</p>
                            <p class="text-bold  text-center text-bold">
                                <span class="phone_email"><a href="tel:+84983033033"><b>Hotline: 0983.033.033</b></a></span>
                                <br>
                                <br>
                                    <span class="phone_email">
                                        <a href="tel:+840971153153">{{ $sdt or 'Chị Phúc: 0971 153 153' }}</a></span>
                                <br>
                                <span class="phone_email"><a
                                            href="tel:+84982200789">{{ $sdt or "Anh Lộc: 0982.200.789" }}</a></span>
                                <br>
                                <span>
                                    <a class="phone_email"
                                       href="mailto:trungvietsach@gmail.com">Email: {{ $email or "trungvietsach@gmail.com" }}</a></span>
                            </p>
                        </div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.3597305852595!2d106.59936671432119!3d10.860219992264742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752b63f0e6ba17%3A0xd928084ac957a50!2zVGjhu7FjIFBo4bqpbSBWaeG7h3QgU-G6oWNo!5e0!3m2!1svi!2s!4v1526637351654"
                                width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@overwrite