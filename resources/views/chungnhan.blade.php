@extends('layouts.index')

@section('content')
    <section id="abouts" class="">
        <div class="container">
            <div class="row">
                <div class="head_title text-center">
                    <h3 style="color:white;line-height: 1.5;" class="text-uppercase">Giấy chứng nhận đăng ký kinh doanh <br>
                        Chứng Nhận Vệ Sinh An Toàn Thực Phẩm</h3>
                </div>
                <div class="main_features_content_area">
                    <div id="links" class="links" style="margin-top: -60px;">
                        <div class="col-md-3" id="chung-nhan">
                            <a href="{{ asset('chungnhan/dkkd-1.jpg') }}" title="Delicate Arch Pano May2018"
                               data-gallery="">
                                <img class="img-thumbnail img-responsive" src="{{ asset('chungnhan/dkkd-1.jpg') }}">
                            </a>
                            <br><br>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ asset('chungnhan/dkkd-2.jpg') }}" title="Delicate Arch Pano May2018"
                               data-gallery="">
                                <img class="img-thumbnail img-responsive" src="{{ asset('chungnhan/dkkd-2.jpg') }}">
                            </a>
                            <br><br>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ asset('chungnhan/attp-cty.jpg') }}" title="Delicate Arch Pano May2018"
                               data-gallery="">
                                <img class="img-thumbnail img-responsive" src="{{ asset('chungnhan/attp-cty.jpg') }}">
                            </a>
                            <br><br>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ asset('chungnhan/phu-luc-attp.jpg') }}" title="Delicate Arch Pano May2018"
                               data-gallery="">
                                <img class="img-thumbnail img-responsive" src="{{ asset('chungnhan/phu-luc-attp.jpg') }}">
                            </a>
                            <br><br>
                        </div>
                    </div>
                    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                    <div id="blueimp-gallery" class="blueimp-gallery">
                        <div class="slides"></div>
                        <h3 class="title"></h3>
                        <a class="prev">‹</a>
                        <a class="next">›</a>
                        <a class="close">×</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                    </div>
                    {{--<div class="col-md-3">--}}
                    {{--<div class="single_abouts_text text-center wow slideInLeft" data-wow-duration="1s">--}}
                    {{--<img src="{{ asset('chungnhan/dkkd-1.jpg') }}" alt=""/>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-3">--}}
                    {{--<div class="single_abouts_text wow slideInRight" data-wow-duration="1s">--}}
                    {{--<img src="{{ asset('chungnhan/dkkd-2.jpg') }}" alt=""/>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-3">--}}
                    {{--<div class="single_abouts_text text-center wow slideInLeft" data-wow-duration="1s">--}}
                    {{--<img src="{{ asset('chungnhan/attp-cty.jpg') }}" alt=""/>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-3">--}}
                    {{--<div class="single_abouts_text wow slideInRight" data-wow-duration="1s">--}}
                    {{--<img src="{{ asset('chungnhan/phu-luc-attp.jpg') }}" alt=""/>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </section>
@overwrite




