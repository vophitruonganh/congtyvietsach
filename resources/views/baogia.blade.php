@extends('layouts.index')

@section('content')
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="text-center  wow fadeIn" data-wow-duration="5s">
                    <div class="col-md-12">
                        <div class="head_title text-center">
                            <h3 class="text-uppercase">Bảng báo giá thực phẩm</h3>
                            <br><br><br><br><br>
                            <div id="links" class="links" style="margin-top: -60px;">
                                @foreach($listBaoGia as $baogia)
                                    <div class="col-md-3">
                                        <a href="{{ asset('/storage/bang-gia/'.$baogia->slug.'.'.$baogia->extension) }}" data-gallery=""><img style="box-shadow: 0px 0px 4px 2px #aaa;" class="img-thumbnail" src="{{ asset('storage/bang-gia/'.$baogia->slug.'.'.$baogia->extension) }}"></a>
                                        <br><br><br><br>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="blueimp-gallery" class="blueimp-gallery">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
    </section>
@overwrite


