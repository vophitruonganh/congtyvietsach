<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
    <!--For Plugins external css-->
    <link rel="stylesheet" href="{{ asset('css/animate/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}"/>
    <!--Theme custom css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!--Theme Responsive css-->

    <title>{{ config('app.name',"Việt Sạch") }}</title>
    <style type="text/css">
        a {
            color: #fff;
        }

        #baogias > a > img {
            box-shadow: 0px 0px 2px 1px #444 !important;
        }
    </style>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class='preloader'>
    <div class='loaded'>&nbsp;</div>
</div>
<header id="home" class="navbar-fixed-top">
    <div class="header_top_menu clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call_us_text">
                        {{--<a href=""><i class="fa fa-clock-o"></i> Số điện thoại</a>--}}
                        <a href="tel:+84981153153"><i class="fa fa-phone"></i>Chị Phúc: 0971.153.153</a>
                        <a href="tel:+84982200789"><i class="fa fa-phone"></i>Anh Lộc: 0982.200.789</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End navbar-collapse-->

    <div class="main_menu_bg">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            {{--<a class="navbar-brand our_logo" href="#"><img src="{{ asset('images/logo.png')}}" alt="" /></a>--}}
                            {{--<a class="navbar-brand our_logo" href="#"><h3>Việt Sạch</h3></a>--}}
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-left">
                                <li><h4><a style="color:#fff;" href="">Việt Sạch</a></h4></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#slider"><strong>Trang chủ</strong></a></li>
                                <li><a href="#gioithieu"><strong>Giới thiệu</strong></a></li>
                                <li><a class="booking" href="#portfolio"><strong>Bảng báo giá</strong></a></li>
                                <li><a href="#abouts"><strong>Giấy chứng nhận</strong></a></li>
                                <li><a href="#features"><strong>Thông tin liên hệ</strong></a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</header> <!-- End Header Section -->

<section id="slider" class="slider">
    <div class="slider_overlay">
        <div class="container">
            <div class="row">
                <div class="main_slider text-center">
                    <div class="col-md-12">
                        <div class="main_slider_content wow zoomIn" data-wow-duration="1s">
                            <h1>Thực Phẩm Việt Sạch</h1>
                            <p>Chuyên cung cấp thực phẩm bếp ăn cho trường học, công ty, xí nghiệp, cơ quan</p>
                            <button class="btn-lg"><a class="booking" href="#portfolio"><strong>Bảng báo giá thực
                                        phẩm</strong></a></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section id="gioithieu" class="ourPakeg">
    <div class="container">
        <div class="row" style="color: white !important;">
            <div class="col-md-4">
                <div class="col-md-10"
                     style="background: #fff; color:#000; border-radius: 15px; padding: 20px 20px 20px 20px;  box-shadow: 0px 20px 2px 1px #333;">
                    <div class="text-center">
                        <div class="icon-center"><i style="font-size: 40px !important;"
                                                    class="service-icon fa fa-thumbs-up"></i></div>
                        <br>
                        <h4>Cam kết chất lượng</h4>
                    </div>
                    <p class="text-center" style="line-height:1.5 !important;">
                        Chúng tôi luôn lấy chất lượng và đảm bảo vệ sinh an toàn thực phẩm làm kim chỉ nam trong hoạt
                        đông kinh doanh của chúng tôi.
                        <br><br>
                        Mang lại thực phẩm tốt và sức khóe cho khách hàng là nhiệm vụ của chúng tôi.
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-10"
                     style="margin-top:100px !important; background: #fff; color:#000; border-radius: 15px; padding: 20px 20px 20px 20px; box-shadow: 0px 20px 2px 1px #333;">
                    <div class="text-center">
                        <div class="icon-center"><i style="font-size: 40px !important;"
                                                    class="service-icon fa fa-bookmark"></i></div>
                        <br>
                        <h4>Phương châm kinh doanh</h4>
                    </div>
                    <p class="text-center" style="line-height:1.5 !important;">
                        Chúng tôi luôn dùng uy tín và chất lượng lên hàng đầu và làm tiêu chí cho việc hợp tác và gắn bó
                        lâu dài với khách hàng.
                        <br>
                        <br>
                        Phục vụ khách hàng là công viêc, trách nhiệm của chúng tôi.
                    </p>
                </div>

            </div>
            <div class="col-md-4">
                <div class="col-md-10"
                     style="background: #fff; color:#000; border-radius: 15px; padding: 20px 20px 20px 20px; margin-top: 100px;  box-shadow: 0px 20px 2px 1px #333;">
                    <div class="head_title text-center">
                        <div class="icon-center"><i style="font-size: 40px !important;"
                                                    class="service-icon fa fa-bell"></i></div>
                        <br>
                        <h4>Tiêu chí kinh doanh</h4>
                    </div>
                    <p class="text-center" style="line-height:1.5 !important;">
                        Chúng tôi luôn tiếp nhận ý kiến của khách hàng để nâng cao dịch vụ, chất lượng sản phẩm. Thành
                        công của khách hàng cũng là thành công của chúng tôi.
                        <br><br>
                        Khách hàng và chúng tôi luôn đồng hành cùng pháp triển.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="portfolio" class="portfolio">
    <div class="container">
        <div class="row">
            <div class="text-center  wow fadeIn" data-wow-duration="5s">
                <div class="col-md-12">
                    <div class="head_title text-center">
                        <h3 class="text-uppercase">Bảng báo giá thực phẩm</h3>
                        <br><br><br><br><br>
                        <div id="links" class="links" style="margin-top: -60px;">
                            @foreach($listBaoGia as $baogia)
                                <div class="col-md-3">
                                    <a href="{{ asset('/storage/bang-gia/'.$baogia->slug.'.'.$baogia->extension) }}" data-gallery=""><img style="box-shadow: 0px 0px 4px 2px #aaa;" class="img-thumbnail" src="{{ asset('storage/bang-gia/'.$baogia->slug.'.'.$baogia->extension) }}"></a>
                                    <br><br><br><br>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="abouts" class="">
    <div class="container">
        <div class="row">
            <div class="head_title text-center">
                <h3 style="color:white;line-height: 1.5;" class="text-uppercase">Giấy chứng nhận đăng ký kinh doanh <br>
                    Chứng Nhận Vệ Sinh An Toàn Thực Phẩm</h3>
            </div>
            <div class="main_features_content_area">
                <div id="links" class="links" style="margin-top: -60px;">
                    <div class="col-md-3" id="chung-nhan">
                        <a href="{{ asset('chungnhan/dkkd-1.jpg') }}" title="Delicate Arch Pano May2018"
                           data-gallery="">
                            <img class="img-thumbnail" src="{{ asset('chungnhan/dkkd-1.jpg') }}">
                        </a>
                        <br><br>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ asset('chungnhan/dkkd-2.jpg') }}" title="Delicate Arch Pano May2018"
                           data-gallery="">
                            <img class="img-thumbnail" src="{{ asset('chungnhan/dkkd-2.jpg') }}">
                        </a>
                        <br><br>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ asset('chungnhan/attp-cty.jpg') }}" title="Delicate Arch Pano May2018"
                           data-gallery="">
                            <img class="img-thumbnail" src="{{ asset('chungnhan/attp-cty.jpg') }}">
                        </a>
                        <br><br>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ asset('chungnhan/phu-luc-attp.jpg') }}" title="Delicate Arch Pano May2018"
                           data-gallery="">
                            <img class="img-thumbnail" src="{{ asset('chungnhan/phu-luc-attp.jpg') }}">
                        </a>
                        <br><br>
                    </div>
                </div>
                <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                <div id="blueimp-gallery" class="blueimp-gallery">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
                {{--<div class="col-md-3">--}}
                {{--<div class="single_abouts_text text-center wow slideInLeft" data-wow-duration="1s">--}}
                {{--<img src="{{ asset('chungnhan/dkkd-1.jpg') }}" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3">--}}
                {{--<div class="single_abouts_text wow slideInRight" data-wow-duration="1s">--}}
                {{--<img src="{{ asset('chungnhan/dkkd-2.jpg') }}" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3">--}}
                {{--<div class="single_abouts_text text-center wow slideInLeft" data-wow-duration="1s">--}}
                {{--<img src="{{ asset('chungnhan/attp-cty.jpg') }}" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3">--}}
                {{--<div class="single_abouts_text wow slideInRight" data-wow-duration="1s">--}}
                {{--<img src="{{ asset('chungnhan/phu-luc-attp.jpg') }}" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</section>
<section id="features" class="features">
    <div class="slider_overlay">
        <div class="container">
            <div class="row">
                <div class="main_features_content_area  wow fadeIn" data-wow-duration="3s">
                    <div class="col-lg-12">
                        <div style="background: #000; opacity: 0.7; padding: 15px; border-top-left-radius: 15px; border-top-right-radius: 15px;">
                            <h3 class="text-center text-uppercase">Liên hệ với chúng tôi</h3>
                            <p class="text-bold text-center">{{ $diachi or 'Địa chỉ: K26, Ấp Mỹ Hoà 4, Xã Xuân Thới Đông, huyện Hóc Môn, thành phố Hồ Chí Mính' }}</p>
                            <p class="text-bold  text-center text-bold">
                                    <span class="phone_email">
                                        <a href="tel:+840971153153">{{ $sdt or 'Chị Phúc: 0971 153 153' }}</a></span>
                                <br>
                                <span class="phone_email"><a
                                            href="tel:+84982220789">{{ $sdt or "Anh Lộc: 0982.220.789" }}</a></span>
                                <br>
                                <span>
                                    <a class="phone_email"
                                       href="mailto:trungvietsach@gmail.com">Email: {{ $email or "trungvietsach@gmail.com" }}</a></span>
                            </p>
                        </div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.3597305852595!2d106.59936671432119!3d10.860219992264742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752b63f0e6ba17%3A0xd928084ac957a50!2zVGjhu7FjIFBo4bqpbSBWaeG7h3QgU-G6oWNo!5e0!3m2!1svi!2s!4v1526637351654"
                                width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<section id="mobaileapps" class="mobailapps">--}}
{{--<div class="slider_overlay">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="main_mobail_apps_content wow zoomIn">--}}
{{--<div class="col-md-5 col-sm-12 text-center">--}}
{{--<img src="{{ asset('images/iphone.png')}}" alt=""/>--}}
{{--</div>--}}
{{--<div class="col-md-7 col-sm-12">--}}
{{--<div class="single_monail_apps_text">--}}
{{--<h3> Happy to Announce </h3>--}}
{{--<h1>Mobile App <span>is Available in every OS platform.</span></h1>--}}

{{--<a href=""><img src="{{ asset('images/google.png')}}" alt=""/></a>--}}
{{--<a href=""><img src="{{ asset('images/apps.png')}}" alt=""/></a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}

<section id="footer_widget" class="footer_widget">
    <div class="container">
        <div class="row">
            <div class="footer_widget_content text-center">
                <div class="col-md-4">
                    <div class="single_widget wow fadeIn" data-wow-duration="2s">
                        <h3 style="font-family: Arial !important;">Thông tin liên hệ</h3>
                        <div class="single_widget_info">
                            <p>Địa chỉ: K26 , Ấp Mỹ Hòa 4 , Xã Xuân Thới Đông , Huyện Hóc Môn
                                <span class="phone_email"><a href="tel:+840971153153">Chị Phúc: 0971 153 153</a></span>
                                <span class="phone_email"><a href="tel:+84982220789">Anh Lộc: 0982.220.789</a></span>
                                <span><a class="phone_email" href="mailto:trungvietsach@gmail.com">Email: trungvietsach@gmail.com</a></span>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="single_widget wow fadeIn" data-wow-duration="4s">
                        <h3 style="font-family: Arial !important;">Khách hàng thân thiết</h3>
                        <br>
                        <div>
                            <p>Hệ thống bếp nhà hàng Nhật Hạ 1,2,3</p>
                            <p>Hệ thống bếp ăn công ty Tân Cường Minh</p>
                            <p>Hệ thống trường THPT cơ sở Hòa Bình</p>
                            <p class="text-default">Công ty liên doanh Phạm Asset</p>
                            <p class="text-default">Công ty may Thành Công - Khu công nghiệp Tân Bình</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="single_widget wow fadeIn" data-wow-duration="5s">
                        <h3 style="font-family: Arial !important;">Gửi email</h3>
                        <div class="single_widget_form text-left">
                            <form action="#" id="formid">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name"
                                           placeholder="Họ tên hoặc tên công ty, tổ chức"
                                           required="">
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Email của bạn"
                                           required="">
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Tiêu đề">
                                </div> <!-- end of form-group -->

                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="3"
                                              placeholder="Nội dung"></textarea>
                                </div>

                                <input type="submit" value="click here" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer-->
<footer id="footer" class="footer">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12">
                <div class="copyright wow zoomIn" data-wow-duration="3s">
                    <p>Bản quyền website thuộc Việt Sạch</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>
<link type="text/css" rel="stylesheet" lang="stylesheet" href="{{ asset('css/blueimp-gallery.min.css') }}">
<script src="{{ asset('js/vendor/jquery.js') }}"></script>
<script src="{{ asset('js/blueimp-gallery.js') }}"></script>
<script src="{{ asset('js/jquery.blueimp-gallery.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery-easing/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('js/wow/wow.min.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script type="text/javascript">

</script>
</body>
</html>
