@extends('dashboard')
@section('page')
    <div class="card">
        <div class="card-header bg-success">Trang quản lý</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            Chào {{ Auth::user()->name }}, bạn đã đăng nhập thành công trang quản trị.
        </div>
    </div>
@overwrite