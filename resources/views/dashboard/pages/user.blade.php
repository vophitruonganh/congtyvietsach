@extends('dashboard')
@section('page')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">STT</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Email</th>
                        <th scope="col">Ngày tạo</th>
                        <th scope="col">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($listbaogia))
                        @php $i=0; @endphp
                        @foreach($listbaogia as $baogia)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $baogia->name }}</td>
                                <td>{{ $baogia->email }}</td>
                                <td>{{ date('d/m/Y',strtotime($baogia->created_at)) }}</td>
                                <td class="text-center">
                                    <form action="{{ url('delete-bao-gia') }}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $baogia->id }}" id="bao_gia_id" name="bao_gia_id">
                                        <button type="submit" class="btn btn-danger btn-sm">Xoá</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="5">Chưa có báo giá</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    {{--<div id="modal_upload_bao_gia" class="modal">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header bg-success">--}}
                    {{--<div class="modal-title">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<form class="form-horizontal" action="{{ url('upload-bao-gia') }}" method="post" enctype="multipart/form-data">--}}
                        {{--@csrf--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="file" class="form-control-file" id="bao_gia" name="bao_gia">--}}
                        {{--</div>--}}

                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="submit" class="btn btn-success">Nhâp báo giá</button>--}}
                    {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>--}}
                {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@overwrite
