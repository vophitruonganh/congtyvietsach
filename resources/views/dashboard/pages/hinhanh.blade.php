@extends('dashboard')
@section('page')
    <div class="card">
        {{--<div class="card-header bg-success">Quản lý báo giá</div>--}}

        <div class="card-body">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_upload_bao_gia"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nhập báo gía</button>
            <br><br>

            @if ($errors->has('errors'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('errors') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div id="modal_upload_bao_gia" class="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{--<div class="modal-header bg-success">--}}
                {{--<div class="modal-title">--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="modal-body">
                    <form class="form-horizontal" action="{{ route('hinhanh.upload') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="file" class="form-control-file" id="bao_gia" name="bao_gia">
                        </div>

                        {{--</div>--}}
                        {{--<div class="modal-footer">--}}
                        <button type="submit" class="btn btn-success">Nhâp báo giá</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                        {{--</div>--}}
                    </form>
                </div>
            </div>
        </div>
@overwrite
