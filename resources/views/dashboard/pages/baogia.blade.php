@extends('dashboard')
@section('page')
    <div class="card">
        {{--<div class="card-header bg-success">Quản lý báo giá</div>--}}

        <div class="card-body">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_upload_bao_gia"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nhập báo gía</button>
            <br><br>

            @if ($errors->has('errors'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('errors') }}</strong>
                </span>
            @endif
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">STT</th>
                        <th scope="col">Tên báo giá</th>
                        <th scope="col">Người đăng</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Ngày tạo</th>
                        <th scope="col">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($listbaogia))
                        @php $i=0; @endphp
                        @foreach($listbaogia as $baogia)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><img class="img-thumbnail img-responsive" style="width: 100px;" src="{{ asset('storage/bang-gia/'.$baogia->slug.'.'.$baogia->extension) }}" alt=""></td>
                                <td>{{ $baogia->name }}</td>
                                <td>{{ $baogia->user()->name }}</td>
                                <td>@if($baogia->enabled == true)
                                        <label class="badge badge-success">Đang sử dụng</label>
                                    @else
                                        <label class="badge badge-danger">Không sử dụng</label>
                                    @endif
                                </td>
                                <td>{{ date('d/m/Y',strtotime($baogia->created_at)) }}</td>
                                <td class="text-center">
                                    <form action="{{ route('baogia.delete') }}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $baogia->id }}" id="bao_gia_id" name="bao_gia_id">
                                        <button type="submit" class="btn btn-danger btn-sm">Xoá</button>
                                    </form>
                                    <form action="{{ route('baogia.updateStatus') }}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $baogia->id }}" id="id" name="id">
                                        @if($baogia->enabled == 0)
                                            <input type="hidden" value="1" id="status" name="status">
                                            <button type="submit" class="btn btn-success btn-sm">Sử dụng</button>
                                        @endif

                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="5">Chưa có báo giá</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="col-md-6 col-md-offset-2">{{ $listbaogia->links() }}</div>
        </div>
    </div>
    <div id="modal_upload_bao_gia" class="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <div class="modal-title">
                    </div>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{ route('baogia.upload') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="file" class="form-control-file" id="bao_gia[]" name="files[]" multiple>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Nhâp báo giá</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                        </div>
                    </form>
                </div>


                <script>
                </script>
            </div>
        </div>
    </div>
@overwrite
