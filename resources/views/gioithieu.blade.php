@extends('layouts.index')

@section('content')
    <section id="gioithieu" class="ourPakeg">
    <div class="container">
        <div class="row" style="color: white !important;">
            <div class="col-md-12">
                <div class="text-center" style='background:#fff; color:#333; margin-top:40px; padding:20px 20px 40px 20px; border-radius:30px; background: #68543F; opacity: 0.9; color:#fff; box-shadow: 0px 0px 20px 4px inset #111; line-height: 2 !important; font-family: "Roboto", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"'>
                    <div class="icon-center">
                        {{--<i style="font-size: 40px !important;" class="service-icon fa fa-star"></i></div>--}}
                        <br>
                        <h4 style="margin-bottom: 40px;"> Giới thiệu</h4>

                        <p class="text-center"><strong>THỰC PHẨM VIỆT SẠCH</strong> chuyên cung cấp thực phẩm tươi, đông lạnh, gia vị, trứng gia cầm và các mặt hàng rau củ quả,rau sạch <br>tất cả các mặt hàng RAU CỦ QUẢ cho nhà hàng, quán ăn, công ty, căn tin, xí nghiệp,  bếp ăn trường học.
                            <br><br>
                            Với kinh nghiệm nhiều năm hoạt động trong lĩnh vực kinh doanh thực phẩm tươi sống <br> cùng với dịch vụ trọn gói và giao hàng tận nơi.
                            <br><br><strong>Thực Phẩm Việt Sạch</strong> tin tưởng sẽ đem đến cho khách hàng những sản phẩm <strong>"CHẤT LƯỢNG – AN TOÀN – VỆ SINH & TIẾT KIỆM"</strong><br>đảm bảo mang lại sự hài lòng và an tâm tuyệt đối cho Quý khách hàng.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-10"
                         style="margin-top:100px; background: #fff; color:#000; border-radius: 15px; padding: 20px 20px 20px 20px;  box-shadow: 0px 20px 2px 1px #333;">
                        <div class="text-center">
                            <div class="icon-center"><i style="font-size: 40px !important;"
                                                        class="service-icon fa fa-thumbs-up"></i></div>
                            <br>
                            <h4>Cam kết chất lượng</h4>
                        </div>
                        <p class="text-center" style="line-height:1.5 !important;">
                            Chúng tôi luôn lấy chất lượng và đảm bảo vệ sinh an toàn thực phẩm làm kim chỉ nam trong hoạt
                            đông kinh doanh của chúng tôi.
                            <br><br>
                            Mang lại thực phẩm tốt và sức khóe cho khách hàng là nhiệm vụ của chúng tôi.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-10"
                         style="margin-top:100px !important; background: #fff; color:#000; border-radius: 15px; padding: 20px 20px 20px 20px; box-shadow: 0px 20px 2px 1px #333;">
                        <div class="text-center">
                            <div class="icon-center"><i style="font-size: 40px !important;"
                                                        class="service-icon fa fa-bookmark"></i></div>
                            <br>
                            <h4>Phương châm kinh doanh</h4>
                        </div>
                        <p class="text-center" style="line-height:1.5 !important;">
                            Chúng tôi luôn dùng uy tín và chất lượng lên hàng đầu và làm tiêu chí cho việc hợp tác và gắn bó
                            lâu dài với khách hàng.
                            <br>
                            <br>
                            Đáp ứng nhu cầu khách hàng là công viêc, trách nhiệm của chúng tôi.
                        </p>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="col-md-10"
                         style="background: #fff; color:#000; border-radius: 15px; padding: 20px 20px 20px 20px; margin-top: 100px;  box-shadow: 0px 20px 2px 1px #333;">
                        <div class="text-center">
                            <div class="icon-center"><i style="font-size: 40px !important;"
                                                        class="service-icon fa fa-bell"></i></div>
                            <br>
                            <h4>Tiêu chí kinh doanh</h4>
                        </div>
                        <p class="text-center" style="line-height:1.5 !important;">
                            Chúng tôi luôn tiếp nhận ý kiến của khách hàng để nâng cao dịch vụ, chất lượng sản phẩm. Thành
                            công của khách hàng cũng là thành công của chúng tôi.
                            <br><br>
                            Khách hàng và chúng tôi luôn đồng hành cùng phát triển.</p>
                    </div>
                </div>
            </div>
        </div>
</section>

@overwrite