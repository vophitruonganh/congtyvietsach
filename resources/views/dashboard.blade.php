@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="list-group">
                <a href="{{ route('baogia') }}" class="list-group-item list-group-item-action active bg-success border-success">
                    Quản lý báo giá
                </a>
                {{--<a href="{{ route('giaodien') }}" class="list-group-item list-group-item-action">Quản lý giao diện--}}
                {{--</a>--}}
                {{--<a href="{{ route('hinhanh') }}" class="list-group-item list-group-item-action">Quản lý hình ảnh--}}
                {{--</a>--}}
                {{--<a href="{{ route('users') }}" class="list-group-item list-group-item-action disabled">Quản lý người dùng--}}
                {{--</a>--}}
            </div>
        </div>
        <div class="col-md-9">
            @yield('page')
        </div>
    </div>
</div>
@endsection
