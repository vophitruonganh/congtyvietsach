<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
    <!--For Plugins external css-->
    <link rel="stylesheet" href="{{ asset('css/animate/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}"/>
    <!--Theme custom css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!--Theme Responsive css-->

    <title>{{ config('app.name',"Việt Sạch") }}</title>
    <style type="text/css">
        a {
            color: #fff;
        }

        #baogias > a > img {
            box-shadow: 0px 0px 2px 1px #444 !important;
        }
    </style>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class='preloader'>
    <div class='loaded'>&nbsp;</div>
</div>
<header id="home" class="navbar-fixed-top">
    <div class="header_top_menu clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call_us_text">
                        {{--<a href=""><i class="fa fa-clock-o"></i> Số điện thoại</a>--}}
                        <a href="tel:+84981153153"><i class="fa fa-phone"></i>Chị Phúc: 0971.153.153</a>
                        <a href="tel:+84982200789"><i class="fa fa-phone"></i>Anh Lộc: 0982.200.789</a>
                        <a href="tel:+84983033033"><i class="fa fa-phone"></i>Hotline: 0983.033.033</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End navbar-collapse-->

    <div class="main_menu_bg">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-left">
                                <li><h4><a style="color:#fff;" href="">Việt Sạch</a></h4></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li ><a @if( $action_name == 'index') style="color: #da9b31;" @endif href="{{ url('/') }}"><strong>Trang chủ</strong></a></li>
                                <li  ><a @if( $action_name == 'gioithieu') style="color: #da9b31;" @endif href="{{ url('/gioithieu') }}"><strong>Giới thiệu</strong></a></li>
                                <li><a @if( $action_name == 'baogiasanpham') style="color: #da9b31;" @endif class="booking" href="{{ url('/baogiasanpham') }}"><strong>Bảng báo giá</strong></a></li>
                                <li><a @if( $action_name == 'giaychungnhan') style="color: #da9b31;" @endif href="{{ url('/giaychungnhan') }}"><strong>Giấy chứng nhận</strong></a></li>
                                <li><a @if( $action_name == 'lienhe') style="color: #da9b31;" @endif href="{{ url('/lienhe') }}"><strong>Thông tin liên hệ</strong></a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</header> <!-- End Header Section -->

<section id="slider" class="slider">
    <div class="slider_overlay">
        <div class="container">
            <div class="row">
                <div class="main_slider text-center">
                    <div class="col-md-12">
                        <div class="main_slider_content wow zoomIn" data-wow-duration="1s">
                            <h1>Thực Phẩm Việt Sạch</h1>
                            <p class="text-body" style="font-weight: bold;">Chuyên Cung Cấp thực phẩm tươi sạch, thịt cá, rau củ quả các loại <br>Suất ăn công nghiệp, khối doanh nghiệp, nhà máy tại khu công nghiệp, bệnh viện, trường học</p>
                            <button class="btn-lg"><a class="booking" href="{{ url('baogiasanpham') }}"><strong>Bảng báo giá thực
                                        phẩm</strong></a></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@yield('content')
<section id="footer_widget" class="footer_widget">
    <div class="container">
        <div class="row">
            <div class="footer_widget_content text-center">
                <div class="col-md-4">
                    <div class="single_widget wow fadeIn" data-wow-duration="2s">
                        <h3 style="font-family: Arial !important;">Thông tin liên hệ</h3>
                        <div class="single_widget_info">
                            <p>Địa chỉ: K26 , Ấp Mỹ Hòa 4 , Xã Xuân Thới Đông , Huyện Hóc Môn
                                <span class="phone_email">Giờ mở cửa: 8 giờ - 18 giờ (các ngày trong tuần)</span>
                                <span class="phone_email"><a href="tel:+84983033033">Hotline: 0983.033.033</a></span>
                                <span class="phone_email"><a href="tel:+840971153153">Chị Phúc: 0971 153 153</a></span>
                                <span class="phone_email"><a href="tel:+84982200789">Anh Lộc: 0982.200.789</a></span>
                                <span><a class="phone_email" href="mailto:trungvietsach@gmail.com">Email: trungvietsach@gmail.com</a></span>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="single_widget wow fadeIn" data-wow-duration="4s">
                        <h3 style="font-family: Arial !important;">Khách hàng thân thiết</h3>
                        <br>
                        <div>
                            <p>Hệ thống bếp nhà hàng Nhật Hạ 1,2,3</p>
                            <p>Hệ thống bếp ăn công ty Tân Cường Minh</p>
                            <p>Hệ thống trường THPT cơ sở Hòa Bình</p>
                            <p class="text-default">Công ty Liên Doanh Phạm Asset</p>
                            <p class="text-default">Công ty may Thành Công - Khu công nghiệp Tân Bình</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="single_widget wow fadeIn" data-wow-duration="5s">
                        <h3 style="font-family: Arial !important;">Gửi email</h3>
                        <div class="single_widget_form text-left">
                            <form action="#" id="formid">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name"
                                           placeholder="Họ tên hoặc tên công ty, tổ chức"
                                           required="">
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Email của bạn"
                                           required="">
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Tiêu đề">
                                </div> <!-- end of form-group -->

                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="3"
                                              placeholder="Nội dung"></textarea>
                                </div>

                                <input type="submit" value="click here" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer-->
<footer id="footer" class="footer">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12">
                <div class="copyright wow zoomIn" data-wow-duration="3s">
                    <p>Bản quyền website thuộc Việt Sạch</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>
<link type="text/css" rel="stylesheet" lang="stylesheet" href="{{ asset('css/blueimp-gallery.min.css') }}">
<script src="{{ asset('js/vendor/jquery.js') }}"></script>
<script src="{{ asset('js/blueimp-gallery.js') }}"></script>
<script src="{{ asset('js/jquery.blueimp-gallery.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery-easing/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('js/wow/wow.min.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script type="text/javascript">

</script>
</body>
</html>
