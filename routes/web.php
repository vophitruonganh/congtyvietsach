<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('index');
//});

Route::get('/',"IndexController@index");
Route::get('/gioithieu',"IndexController@gioithieu");
Route::get('/baogiasanpham',"IndexController@baogia");
Route::get('/lienhe',"IndexController@lienhe");
Route::get('/giaychungnhan',"IndexController@giaychungnhan");

Auth::routes();

Route::group(['prefix' => 'dashboard'],function(){
//    Route::get('/',"HomeController@index");
    Route::get('/',"ReportController@index");
    Route::get('/baogia', 'ReportController@index')->name('baogia');
    Route::post('/baogia/delete', 'ReportController@deletebaogia')->name('baogia.delete');
    Route::post('/baogia/upload', 'ReportController@uploadbaogia')->name('baogia.upload');
    Route::post('/baogia/update-status', 'ReportController@updateStatus')->name('baogia.updateStatus');

    Route::get('/hinhanh', 'AttachmentController@index')->name('hinhanh');
    Route::get('/giaodien', 'LayoutController@index')->name('giaodien');
    Route::get('/hinhanh/upload', 'AttachmentController@uploadImages')->name('hinhanh.upload');
    Route::get('/users', 'UserController@index')->name('users');

});