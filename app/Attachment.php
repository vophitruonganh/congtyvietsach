<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Attachment extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'attachments';
    protected $fillable = [
        'user_id',
        'slug',
        'extension',
        'name',
        'path',
        'uri',
        'mimeType',
        'size',
        'width',
        'height',
        'enabled',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user(){
        return $this->hasOne("\App\User",'id','user_id')->first();
    }
}
