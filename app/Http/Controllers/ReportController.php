<?php

namespace App\Http\Controllers;

use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listbaogia = Attachment::orderBy('created_at','desc')
                                ->paginate(24);

        $this->notifySlack($request->ip() . ' - Goto home page');
        return view('dashboard.pages.baogia')
            ->with('listbaogia',$listbaogia);
    }

    public function updateStatus(Request $request){
        try{
            $params = $request->all();
            $id = isset($params['id'])?$params['id']:0;
            $status = isset($params['status'])?$params['status']:false;
            DB::beginTransaction();
            if ($id){

                Attachment::where('id',"!=",$id)->update(['enabled' => 0]);
                Attachment::where('id',$id)->update(['enabled' => $status]);
            }
            DB::commit();
            return redirect()->back();
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function uploadbaogia(Request $request){
        try{

            $files = $request->allFiles();
            if ($files){
                DB::beginTransaction();
                foreach($files['files'] as $file){
                    $fileName = md5(time() .'_'. Str::random());
                    $fileExtension = $file->getClientOriginalExtension();
                    $mimeType = $file->getMimeType();
                    $size = $file->getSize();
                    $name = $file->getClientOriginalName();
                    if ($fileExtension == 'jpg' || $fileExtension == 'jpeg' || $fileExtension == 'png' ){
                        Storage::disk('local')->put('public/bang-gia/'.$fileName.'.'.$fileExtension,file_get_contents($file->getRealPath()));
                        Attachment::where('mimeType','application/pdf')->update(['enabled' => false]);
                        Attachment::create(
                            array(
                                'user_id' => Auth::user()->id,
                                'slug' => $fileName,
                                'extension' => $fileExtension,
                                'name' => $name,
                                'mimeType' => $mimeType,
                                'path' => '',
                                'uri' => '',
                                'size' => $size,
                                'width' => 0,
                                'height' => 0,
                                'enabled' => true
                            )
                        );
                    }else{
                        return redirect()->back()->withErrors(array('errors' => 'Chỉ hỗ trợ file PDF'));
                    }
                }
            }
            DB::commit();

            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => "Không tìm thấy báo giá"));
            }else{
                return redirect()->back();
            }
        }catch (\Exception $e){
            DB::rollBack();
            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => $e->getMessage()));
            }
            return $e->getMessage();
        }
    }

    public function deletebaogia(Request $request){
        try{
            $params = $request->all();
            $idbaogia = $params['bao_gia_id'];
            $baogia = Attachment::find($idbaogia);
            if ($idbaogia && $baogia){
//                if ($baogia['enabled'] == false){
                    Attachment::destroy($params['bao_gia_id']);
//                }else{
//                    return redirect()->back()->withErrors(array(['errors' => "Báo giá đang được sử dụng"]));
//                }
            }

            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => "Không tìm thấy báo giá"));
            }else{
                return redirect()->back();
            }
        }catch (\Exception $e){
            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => $e->getMessage()));
            }
            return redirect()->back();
        }
    }
}
