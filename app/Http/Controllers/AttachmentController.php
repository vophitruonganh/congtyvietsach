<?php

namespace App\Http\Controllers;

use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listbaogia = Attachment::where('mimeType',"<>",'application/pdf')->orderBy('created_at','desc')->get();
        return view('dashboard.pages.hinhanh')
            ->with('listbaogia',$listbaogia);
    }

    private function uploadImages(Request $request){
        try{
            $file = $request->files('image');
            if ($file){
                $fileName = time();
                $fileExtension = $file->getClientOriginalExtension();
                $mimeType = $file->getMimeType();
                $size = $file->getSize();
                $name = $file->getClientOriginalName();
                if ($fileExtension = 'jpg' || $fileExtension = 'png' || $fileExtension = 'jpeg'  ){
                    Storage::disk('local')->put('public/images/'.$fileName.'.'.$fileExtension,file_get_contents($file->getRealPath()));
                    DB::beginTransaction();
                    Attachment::create(
                        array(
                            'user_id' => Auth::user()->id,
                            'slug' => $fileName,
                            'extension' => $fileExtension,
                            'name' => $name,
                            'mimeType' => $mimeType,
                            'path' => '',
                            'uri' => '',
                            'size' => $size,
                            'width' => 0,
                            'height' => 0,
                            'enabled' => true
                        )
                    );
                }else{
                    return redirect()->back()->withErrors(array('errors' => 'Chỉ hỗ trợ upload hinh anh'));
                }
                DB::commit();
            }

            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => "Không tìm thấy báo giá"));
            }else{
                return redirect()->back();
            }
        }catch (\Exception $e){
            DB::rollBack();
            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => $e->getMessage()));
            }
            return $e->getMessage();
        }
    }

    public function deletebaogia(Request $request){
        try{
            $params = $request->all();
            $idbaogia = $params['bao_gia_id'];

            if ($idbaogia){
                Attachment::destroy($params['bao_gia_id']);
            }

            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => "Không tìm thấy báo giá"));
            }else{
                return redirect()->back();
            }
        }catch (\Exception $e){
            if($request->ajax()){
                return response()->json(array('status' => 0, 'msg' => $e->getMessage()));
            }
            return redirect()->back();
        }
    }
}
