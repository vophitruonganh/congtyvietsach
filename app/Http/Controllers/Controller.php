<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Ixudra\Curl\Facades\Curl;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $hookSlack = 'https://hooks.slack.com/services/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';

    public function notifySlack($message, $channel = 'debug-cms-internal')
    {
        if (is_array($message)) {
            $message = json_encode($message);
        }
//        $params ='payload={"text": "A very important thing has occurred! <https://alert-system.com/alerts/1234|Click here> for details!"}';
        $listUserName = array('Jiren', 'Goku', 'Gohan', 'Goten', 'Mabu', 'Satan', 'Bumma');
        $listIcon = array(":monkey_face:", ":slack:", ":ghost:", ":monkey_face:", ":dog:", ":cow:");
        $params = array(
            'text' => $message,
            'username' => $listUserName[rand(0, 6)],
            "icon_emoji" => $listIcon[rand(0, 5)],
            "channel" => $channel,
        );

        Curl::to($this->hookSlack)
            ->withData($params)
            ->asJson(true)
            ->post();
    }
}
