<?php

namespace App\Http\Controllers;

use App\Attachment;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bangGia = Attachment::where('enabled',true)->orderBy('created_at','desc')->get();
        $this->notifySlack($request->ip() . ' - Goto home page');
        return view('gioithieu')
            ->with('listbaogia',$bangGia);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lienhe()
    {
        $bangGia = Attachment::where('enabled',true)->orderBy('created_at','desc')->get();
        return view('lienhe')
            ->with('listbaogia',$bangGia);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function baogia()
    {
        $bangGia = Attachment::where('enabled',true)->orderBy('created_at','desc')->get();
        return view('baogia')
            ->with('listBaoGia',$bangGia);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function giaychungnhan()
    {
        $bangGia = Attachment::where('enabled',true)->orderBy('created_at','desc')->get();
        return view('chungnhan')
            ->with('listbaogia',$bangGia);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gioithieu()
    {
        $bangGia = Attachment::where('enabled',true)->orderBy('created_at','desc')->get();
        return view('gioithieu')
            ->with('listbaogia',$bangGia);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
